export const typeDef = `
    extend type Query {
        getAllPersons: [Person]
        findId(id: ID): Person
    }
    extend type Mutation {
        createPerson(input: PersonInput): Person
        updatePerson(id: ID!, input: PersonInput): Boolean
    }

    type Person {
        id: ID,
        name: String,
        age: String,
        gender: String,

    }
    
    input PersonInput {
        name: String,
        age: String,
        gender: String,

    }
`;