import { PersonModel } from '../../models/person';

export const resolver = {
    Query: {
        getAllPersons: () => PersonModel.getAllPersons(),
        findId: (_,{id}) => PersonModel.findId(id),
    },
    Mutation: {
        createPerson(root, { input }) {
            return PersonModel.createPerson(input);
        },
        updatePerson(root, { id, input }) {
            if (id) {
                return PersonModel.updatePerson(id, input);
            }
            return false;
        },
    }
}
