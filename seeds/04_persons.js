exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('persons').truncate()
    .then(function () {
      // Inserts seed entries
      return knex('persons').insert([
        { name: "huhur", age: "24", gender: "male",  created_at: knex.fn.now(), updated_at: knex.fn.now()},
        { name: "hahar", age: "35", gender: "female", created_at: knex.fn.now(), updated_at: knex.fn.now()},
      ]);
    });
};